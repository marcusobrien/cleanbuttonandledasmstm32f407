    PRESERVE8
    THUMB
    AREA    |.text|, CODE, READONLY
    ALIGN
    EXPORT SystemInit 
    EXPORT mymain    
    EXPORT EXTI0_IRQHandler
    EXPORT SysTick_Handler
    
PORTD			    EQU     0x0C00			; PortD Offset    
BLUELED	            EQU	    0x8000			; Blue Led 
GPIOA_MODER	        EQU		0x40020000		; Gpio_A Base
RCC_CR		        EQU 	0x40023800		; Rcc Base
RNG_CR		        EQU 	0x50060800		; Rng Base

EXTI_BASE           EQU     0x40013C00
NVIC_BASE           EQU     0xE000E100
NVIC_ISER0          EQU     NVIC_BASE
NVIC_ICER0          EQU     NVIC_BASE  + 0x80
NVIC_ICPR0          EQU     NVIC_BASE  + 0x180
EXTI_IMR            EQU     EXTI_BASE
EXTI_PR0            EQU     EXTI_BASE +  0x14
EXTI_RTSR           EQU     EXTI_BASE + 0x08
EXTI_FTSR           EQU     EXTI_BASE + 0x0C
SYSCFG_EXICR1_EXTI0 EQU     0xF
SYSCFG_BASE         EQU     0x40013800
SYSCFG_EXTICR1      EQU     SYSCFG_BASE + 0x08
SYSCFGEN            EQU     1 << 14
RCC_BASE            EQU     0x40023800
RCC_APB2ENR         EQU     RCC_BASE + 0x44
MR0                 EQU     1
TR0                 EQU     1
EXTI0_IRQn          EQU     6
SETENA6             EQU     1 << EXTI0_IRQn

;TIMER SPECIFIC VARS
Freq_Quartz         EQU 8000000
Freq_Request        EQU 100
SysTick_CTRL        EQU 0xe000e010
SysTick_LOAD        EQU 0xe000e014
SysTick_VAL_ADD     EQU 0xe000e018
SysTick_CALIB       EQU 0xe000e01C
Debounce_Val        EQU 1000    ;200 = 1 second     1 = 5ms 


    ALIGN
    
SystickSetPeriod PROC
    LDR     R1,=Freq_Quartz ; Read clock frequency
    SDIV    R2,R1,R0 ; Calculation of number N of cycles for desired freq.
    SUBS    R2,R2,#1 ; Modification of value N � 1
    LDR     R3,=SysTick_LOAD ; of reload register
    STR     R2,[R3]
    BX      LR
    ENDP

;� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �
Start_SysTick PROC
    PUSH    {LR}
    LDR     R0,=Freq_Request ; Load value of requested period   - *10MS
    LDR     R1,=SysTick_VAL_ADD
    STR     R0,[R1]
    BL      SystickSetPeriod ; Initialization of reload
    LDR     R0,=SysTick_CTRL
    LDR     R1,[R0]
    ORR     R1,R1,#4 ; Choice of clock
    ORR     R1,R1,#2 ; Authorization of interrupt
    ORR     R1,R1,#1 ; Timer launch
    STR     R1,[R0]
    POP     {PC}
    ENDP
    
;� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �
Stop_Systick PROC
    LDR     R0,=SysTick_CTRL
    LDR     R1,[R0]
    BIC     R1,R1,#2 ; Deactivates systick interrupt
    BIC     R1,R1,#1 ; Stops the timer
    STR     R1,[R0]
    BX      LR
    ENDP
    
;� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �
SysTick_Handler PROC
    LDR     R0,=DEBOUNCE_CURRENT
    LDR     R1, [R0]
    CMP     R1, #0
    PUSH    {LR}    
    BNE     Debounce_Handler 
    POP     {PC}
    ENDP

;Only called when we are in a debounce and systick handler 
;is executing - r0 should contain address of debounce value
;Careful as the stack has the LR on it to return from handler
;so leave it as you found it
Debounce_Handler PROC   
    LDRH    R1,[R0]
    SUB     R1,#1 ; Decrement
    STRH    R1,[R0] ; and storage
    CMP     R1, #0
    BEQ     DEBOUNCE_END
    POP     {PC} ;will return out of handler - not back to calling systick handler
DEBOUNCE_END
    ;Enable interrupt again for button
    BL      EnableButtonINT
    BL      Stop_Systick
    BL      ClearPendingAndActiveInterrupt
    POP     {PC} ;will return out of handler - not back to calling systick handler
    ENDP
        
TOGGLE_LED PROC
    LDR     r0,[r11,#0x14+PORTD]		; R0 < @GPIOD_ODR 
    EOR	    r0,r0,	 #BLUELED		; R0 = Toggle Blue Led		     
    STR     r0,[r11,#0x14+PORTD]		; R0 > @GPIOD_ODR
    BX      LR
    ENDP

EXTI0_IRQHandler PROC
    PUSH    {LR}
    BL      DisableButtonINT
    BL      TOGGLE_LED        

    BL      Start_Debounce
    BL      Start_SysTick     ;Debounce needed - use system tick    
    BL      ClearPendingAndActiveInterrupt
    
    POP     {PC}    
    ENDP
    
ClearPendingAndActiveInterrupt PROC
    ;Dont think I need this
    LDR     r0, =NVIC_ICPR0         ;clear pending interrupt from EXTI0_IRQn channel 
    LDR     r1, =SETENA6
    STR     r1, [r0]
    
    ;Need to clear the pending register external line 
    LDR     r0, =EXTI_PR0           
    MOVW    r1, #MR0
    STR     r1, [r0]  

    ;READ VALUE back from system    
    LDR r0, =NVIC_ICPR0
    LDR r1, =SETENA6
    STR r1, [r0]
    
    ;The external peripheral interrupt used by pushbutton
    ;might have a write buffer, so lets force it through to peripheral
    BX      LR
    ENDP
    
Start_Debounce PROC
    PUSH    {LR}
    ;Set the debounce to default value
    LDR     R0, =DEBOUNCE_CURRENT
    LDR     R1, =Debounce_Val
    STR     R1, [R0]
    POP     {PC}
    ENDP
    
EnableButtonINT PROC
    ;Enables the interrupt for the blue pushbutton
    LDR     r0, =NVIC_ISER0         ;set enable interrupt first group, bit 6 means interrupt 6 - from EXTI0_IRQn channel 
    LDR     r1, =SETENA6
    STR     r1, [r0]
    BX      LR
    ENDP    

DisableButtonINT PROC
    ;Disable the interrupt for the blue pushbutton
    LDR     r0, =NVIC_ICER0         ;clear ENABLE interrupt first group, bit 6 means interrupt 6 - from EXTI0_IRQn channel 
    LDR     r1, =SETENA6
    STR     r1, [r0]
    BX      LR
    ENDP    
    
SystemInit FUNCTION
    PUSH    {LR}
    CPSIE I                     ; Enable Interrupts
    LDR     r1,=RCC_BASE	    		; R1 = RCC_CR Base
	MOVW    r0,#0x0009            	; Enable Clock for GPIOs PORTA & PORTD        
	STR     r0,[r1,#0x30]			; R0 > @RCC_AHB1ENR
													         
    LDR     r11,=GPIOA_MODER	    ; R11 = GPIOA_MODER Base			           		
	LDR     r0, =0x40000000     	; GPIOD 15 - General Output - ?Purpose Push-Pull OutPut @ 2Mhz 
	STR     r0, [r11,#0x00+PORTD]		; R0 > @GPIOD_MODER	 Sets the gpiod mode reg to Gen Purp Output
    BL      exti_setup
    POP     {PC}
    ENDFUNC

mymain  FUNCTION
    B .
    ENDFUNC
    
exti_setup FUNCTION
    PUSH    {LR} 
    LDR     r0, =RCC_APB2ENR        ;peripheral clock enable bus for APB2 used for 
                                    ;sys cfg controller and to configure external 
                                    ;interrupt line source, see table in STM32 manual
                                    ;0x40023844    RCC_APB2ENR
    LDR     r1, [r0]
    ORR     r1, #SYSCFGEN           ; Enable System CFG Enable bit [14]
    STR     r1, [r0]

    LDR     r0, =SYSCFG_EXTICR1     ;choosing PA0 as a source for ext. interrupt
    LDR     r1, [r0]                ; contents of address 4001 3808 (sys cfg)
    BIC     r1, #SYSCFG_EXICR1_EXTI0    ;clear bits 3:0 of EXTI0, gives  PA0
    STR     r1, [r0]            ;sets the cleared bits 3:0 to sys cfg EXTI CR1 reg 

    LDR     r0, =EXTI_IMR           ;unset (1) interrupt mask for line 0 - IMR line 0
    MOVW    r1, #MR0
    STR     r1, [r0]

    LDR     r0, =EXTI_RTSR          ;configuring interrupt to be generated on rising edge 
    MOVW    r1, #TR0
    STR     r1, [r0]
    
    ;LDR     r0, =EXTI_FTSR          ;configuring interrupt to be generated also on falling edge 
    ;MOVW    r1, #TR0
    ;STR     r1, [r0]
    
    BL      EnableButtonINT 

    POP     {PC}
    ENDFUNC	        

    AREA  variables, DATA, READWRITE
ALIGN
DEBOUNCE_CURRENT      =     0 ;= is the Same as DCB
	END	