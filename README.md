# README #

### What is this repository for? ###

* Bare metal (no RTOS/scheduler etc), simple SMT32 F407VG button poller in assembly. Simple startup and main in assembly V7M for ARM M4. Single threaded poller and LED toggler.

* Version 1.0

### How do I get set up? ###

This is for the uVision IDE, simple setup (set to stm link etc), no scatter file etc. Plug in the STM32 F4 whatever and download and run.

### Contribution guidelines ###

No tests etc.

### Who do I talk to? ###

marcusobrien@yahoo.com